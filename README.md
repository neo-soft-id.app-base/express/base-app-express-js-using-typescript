# Base App Express JS using TypeScript

## Description

This repository serves as a base application built on Express.js using TypeScript. It provides a structured foundation for building web applications with Node.js, Express, and MySQL database.

## Requirements

- Node.js (v20.9.0)
- Express.js (v4.19.2)
- MySQL

## Installation

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/neo-soft-id.app-base/express/base-app-express-js-using-typescript.git
    ```

2. Install dependencies:

    ```bash
    npm install
    ```

3. Set up MySQL database and configure connection details in 
    ```bash
    `base-app-express-js-using-typescript\.env`.
    ```
4. Set up Migration connection in 
    ```bash
    `base-app-express-js-using-typescript\src\config\migrate-config.js`.
    ```
5. Do it in command:

    ```bash
    npx sequelize-cli db:migrate
    npx sequelize-cli db:seed:all
    ```
6. Start the application:

    ```bash
    npm start
    ```
for local development:
     `npm run dev`

## Migration


1. Create new migration or seeder:
    ```bash
    npx sequelize-cli migration:generate --<migration-file-name>
    npx sequelize-cli seed:generate --name <migration-file-name>
    ```
2. Do migrate with specifiy file:
    ```bash
    npx sequelize-cli db:migrate --to <migration-file-name>
    npx sequelize-cli db:seed --seed <seed-file-name>

3. Or do migration with all option:
    ```bash
    npx sequelize-cli db:migrate
    npx sequelize-cli db:seed:all
    ```

## Features

- **TypeScript Support**: Utilizes TypeScript for static typing and improved developer experience.
- **Express.js Framework**: Built on top of Express.js for robust and scalable web applications.
- **MySQL Database Integration**: Interacts with MySQL database for storing and retrieving data.

## Usage

- **Development**: Run `npm run dev` for development mode with auto-reloading using nodemon.
- **Production**: Run `npm start` to start the application in production mode.

## Author

This project created by **[Sandika Gusti Prakasa](https://linkedin.com/in/sandikagustiprakasa/)**

