"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(QueryInterface, Sequelize) {
    await QueryInterface.createTable("ms_user", {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      userCode: {
        type: Sequelize.STRING(35),
        allowNull: false,
        unique: true,
      },
      firstName: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      lastName: {
        type: Sequelize.STRING(50),
        allowNull: true,
        defaultValue: null,
      },
      username: {
        type: Sequelize.STRING(30),
        allowNull: false,
        unique: true,
      },
      password: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      phoneNumber: {
        type: Sequelize.STRING(20),
        allowNull: true,
        defaultValue: null,
      },
      email: {
        type: Sequelize.STRING(40),
        allowNull: false,
      },
      genderId: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      birthPlace: {
        type: Sequelize.STRING(100),
        allowNull: true,
        defaultValue: null,
      },
      birthDay: {
        type: Sequelize.DATEONLY,
        allowNull: true,
        defaultValue: null,
      },
      address: {
        type: Sequelize.TEXT,
        allowNull: true,
        defaultValue: null,
      },
      isActive: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
    });
  },

  async down(QueryInterface, Sequelize) {
    await QueryInterface.dropTable("ms_user");
  },
};
