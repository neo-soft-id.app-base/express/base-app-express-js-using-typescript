'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable("ms_feature", {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      featureCode: {
        type: Sequelize.STRING(35),
        allowNull: false,
        unique: true
      },
      featureName: {
        type: Sequelize.STRING(30),
        allowNull: false,
      },
      reading: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      creating: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      updating: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      deleting: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      activating: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      inactivating: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      importing: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      exporting: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      approving: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      rejecting: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      special: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      isActive: {
        type: Sequelize.BOOLEAN,
        allowNull: false
      },
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.dropTable("ms_feature");
  }
};
