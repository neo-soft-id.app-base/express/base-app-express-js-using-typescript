'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable("map_user_role", {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      userCode: {
        type: Sequelize.STRING(35),
        allowNull: false
      },
      roleCode: {
        type: Sequelize.STRING(35),
        allowNull: false
      }
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.dropTable("map_user_role");
  }
};
