'use strict';
const crypto = require('crypto');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    
    const generateMD5 = (str) => {
      return crypto.createHash('md5').update(str).digest('hex');
    };

    const dataExists = await queryInterface.rawSelect('ms_user', {
      where: {
        userCode: 'USER-01-SUPERADMIN'
      }
    }, ['id']);

    if(dataExists) return;
    
    return queryInterface.bulkInsert('ms_user', [
      {
        userCode: 'USER-01-SUPERADMIN',
        firstName: 'Sandika',
        lastName: 'Gusti Prakasa',
        username: 'sandika.prakasa',
        password: generateMD5('Password*1!'),
        phoneNumber: '+6282111734649',
        email: 'sandika.prakasa.dev@gmail.com',
        genderId: true,
        birthPlace: 'Tangerang',
        birthDay: new Date('1997-08-17'),
        address: 'No stored',
        isActive: true
      }
    ], {});
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('ms_user', {
      userCode: 'USER-01-SUPERADMIN'
    }, {});
  }
};
