'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {

    const dataExists = await queryInterface.rawSelect('ms_role', {
      where: {
        roleCode: 'ROLE-01-SUPERADMIN'
      }
    }, ['id']);

    if(dataExists) return;

    return queryInterface.bulkInsert('ms_role', [
      {
        roleCode: 'ROLE-01-SUPERADMIN',
        roleName: 'Superadmin',
        authDirectAddress: "/home",
        isActive: true
      }
    ], {});
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('ms_role', {
      roleCode: 'ROLE-01-SUPERADMIN'
    }, {});
  }
};
