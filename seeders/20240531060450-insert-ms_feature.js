'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {

    const dataExists = await queryInterface.rawSelect('ms_feature', {
      where: {
        featureCode: 'FEAT-01-DEFAULT'
      }
    }, ['id']);

    if(dataExists) return;

    return queryInterface.bulkInsert('ms_feature', [
      {
        featureCode: 'FEAT-01-DEFAULT',
        featureName: 'Home',
        reading: true,
        creating: false,
        updating: false,
        deleting: false,
        activating: false,
        inactivating: false,
        importing: false,
        exporting: false,
        approving: false,
        rejecting: false,
        special: false,
        isActive: true
      }
    ], {});
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('ms_feature', {
      featureCode: 'FEAT-01-DEFAULT'
    }, {});
  }
};
