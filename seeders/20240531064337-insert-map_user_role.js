'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {

    const dataExists = await queryInterface.rawSelect('map_user_role', {
      where: {
        userCode: 'USER-01-SUPERADMIN',
        roleCode: 'ROLE-01-SUPERADMIN'
      }
    }, ['id']);

    if(dataExists) return;

    return queryInterface.bulkInsert('map_user_role', [
      {
        userCode: 'USER-01-SUPERADMIN',
        roleCode: 'ROLE-01-SUPERADMIN'
      }
    ], {});
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('map_user_role', {
      userCode: 'USER-01-SUPERADMIN',
      roleCode: 'ROLE-01-SUPERADMIN'
    }, {});
  }
};
