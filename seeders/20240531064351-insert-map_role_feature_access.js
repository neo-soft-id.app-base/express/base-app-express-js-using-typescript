'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {

    const dataExists = await queryInterface.rawSelect('map_role_feature_access', {
      where: {
        roleCode: 'ROLE-01-SUPERADMIN',
        featureCode: 'FEAT-01-DEFAULT'
      }
    }, ['id']);

    if(dataExists) return;

    return queryInterface.bulkInsert('map_role_feature_access', [
      {
        roleCode: 'ROLE-01-SUPERADMIN',
        featureCode: 'FEAT-01-DEFAULT',
        reading: true,
        creating: false,
        updating: false,
        deleting: false,
        activating: false,
        inactivating: false,
        importing: false,
        exporting: false,
        approving: false,
        rejecting: false,
        special: false
      }
    ], {});
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('map_role_feature_access', {
      roleCode: 'ROLE-01-SUPERADMIN',
      featureCode: 'FEAT-01-DEFAULT'
    }, {});
  }
};
