'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {

    const dataExists = await queryInterface.rawSelect('lk_gender', {
      where: {
        id: {
          [Sequelize.Op.in]: [1,2,3]
        }
      }
    }, ['id']);

    if(dataExists) return;

    return queryInterface.bulkInsert('lk_gender', [
      {
        id: '1',
        genderName: 'Male'
      },
      {
        id: '2',
        genderName: 'Female'
      },
      {
        id: '3',
        genderName: 'Other/Organization'
      }
    ], {});
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('lk_gender', {
      id: { [Sequelize.Op.in]: ['1', '2','3'] }
    }, {});
  }
};
