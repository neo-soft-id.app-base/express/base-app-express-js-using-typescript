import bodyParser from "body-parser";
import "dotenv/config";
import express from "express";

import sequelize from "./config/db-config";
import authRoutes from "./routes/auth.routes";
import featureRoutes from "./routes/feature.routes";
import userRoutes from "./routes/user.routes";
import roleRoutes from "./routes/role.routes";

class App {
  public app: express.Application;
  public port: number;

  constructor() {
    this.app = express();
    this.port = parseInt(process.env.PORT!);

    this.init();
  }

  private config(): void {
    this.app.use(bodyParser.json());
  }

  private initiateRoutes(): void {
    this.app.use("/api/auth", authRoutes);
    this.app.use("/api/user", userRoutes);
    this.app.use("/api/feature", featureRoutes);
    this.app.use("/api/role", roleRoutes);
  }

  private async init() {
    this.config();
    this.initiateRoutes();

    this.app.listen(this.port, () => {
      console.log(`Server is running at http://localhost:${this.port}`);
    });
  }
}

new App();
