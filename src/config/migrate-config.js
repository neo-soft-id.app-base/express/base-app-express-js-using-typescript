module.exports = {
  development: {
    username: "root",
    password: "",
    database: "app_base_test",
    host: "127.0.0.1",
    dialect: "mysql",
    timezone: "+07:00"
  },
  test: {
    username: "",
    password: "",
    database: "",
    host: "",
    dialect: "",
  },
  production: {
    username: "root",
    password: "",
    database: "app_base_test",
    host: "127.0.0.1",
    dialect: "mysql",
    timezone: "+07:00"
  },
};
