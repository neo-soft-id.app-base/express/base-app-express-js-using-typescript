import { Request, Response } from "express";
import {
  AuthSignInResponse,
  initiateAuthSignInResponse,
} from "../dto/auth/auth.dto";
import AuthService from "../services/auth.service";
import { HttpStatusCode } from "../utils/app.const";
import Encryption from "../utils/cryptojs";
import { UserDetailByAuth, UserDetailByAuthRequest } from "../dto/user/user-detail-by-auth.dto";
import AppHelper from "../utils/helpers";
import CustomHttpError from "../utils/custom-httperror";
import { BaseResponse } from "../dto/base.dto";
import { FeatureList } from "../dto/feature/feature-list.dto";
import RoleService from "../services/role.service";

class AuthController {
  private authService: AuthService;
  private roleService: RoleService;

  constructor() {
    this.authService = new AuthService();
    this.roleService = new RoleService();
  }

  public async login(req: Request, res: Response): Promise<void> {
    try {
      const request: UserDetailByAuthRequest = { ...req.body || {}} as UserDetailByAuthRequest
      if (
        [
          request?.username,
          request?.password
        ].some(AppHelper.isHasNoValue)
      ) {
        throw new CustomHttpError(
          "Bad Request: Username and password cannot be empty",
          HttpStatusCode.BAD_REQUEST
        );
      }

      const isAuthenticated = await this.authService.matchAuth(
        request.username,
        request.password
      );
      if(!isAuthenticated){
        throw new CustomHttpError(
          "Invalid credentials.",
          HttpStatusCode.UNAUTHORIZED
        );
      }

      const user: BaseResponse<UserDetailByAuth> | null = await this.authService.getUserDetailByAuth(request);
      if(!user.state || !user?.outputSchema) throw new CustomHttpError(user.message, HttpStatusCode.UNAUTHORIZED);

      const featureList: BaseResponse<FeatureList[]> = await this.roleService.getRoleFeatureAccessList(user.outputSchema.roleCode);
      
      const tokenKey = this.authService.generateToken(request.username);
      const permission = Encryption.set(JSON.stringify(featureList ?? null));
      const AuthSignInResponse: AuthSignInResponse = {
        ...initiateAuthSignInResponse,
        state: true,
        message: "Ok.",
        outputSchema: { 
          token: { type: "bearer", key: tokenKey },
          user: user.outputSchema,
          permission
        },
      };
      res.status(HttpStatusCode.OK).send(AuthSignInResponse);

    } catch(error: any) {
      const authSignInResponse: AuthSignInResponse = {
        ...initiateAuthSignInResponse,
        message: error?.message || "Internal server error.",
      };
      const statusCode = AppHelper.isHttpError(error)? error.statusCode : HttpStatusCode.INTERNAL_SERVER_ERROR;
      res.status(statusCode).send(authSignInResponse);
    }
  }
}

export default AuthController;
