import { Request, Response } from "express";
import { Transaction } from "sequelize";
import sequelize from "../config/db-config";
import {
  FeatureAddEndResult,
  FeatureAddRequest,
  FeatureAddResponse,
  initiateFeatureAddResponse,
} from "../dto/feature/feature-add.dto";
import {
  FeatureList,
  FeatureListEndResult,
  FeatureListRequest,
  FeatureListResponse,
  initiateFeatureListResponse,
} from "../dto/feature/feature-list.dto";
import FeatureService from "../services/feature.service";
import { HttpStatusCode } from "../utils/app.const";
import CustomHttpError from "../utils/custom-httperror";
import AppHelper from "../utils/helpers";
import { FeatureEditEndResult, FeatureEditRequest, FeatureEditResponse } from "../dto/feature/feature-edit.dto";
import { FeatureDeleteEndResult, FeatureDeleteRequest, FeatureDeleteResponse } from "../dto/feature/feature-delete.dto";
import { BaseResponse } from "../dto/base.dto";

class FeatureController {
  private featureService: FeatureService;

  constructor() {
    this.featureService = new FeatureService();
  }

  public async getFeatureList(req: Request, res: Response): Promise<void> {
    try {
      const request: FeatureListRequest = { ...req.query || {}} as unknown as FeatureListRequest;

      if (
        request?.search == null ||
        [request?.page, request?.perPage].some(AppHelper.isHasNoValue)
      ) {
        throw new CustomHttpError(
          "Bad Request: search, page and perPage query string is required.",
          HttpStatusCode.BAD_REQUEST
        );
      }

      const features: BaseResponse<FeatureList[]> = await this.featureService.getFeatureList(request);
      const totalRecords: number = await this.featureService.getFeatureListTotalRecords(request?.search) ?? 0;
      
      if(!features.state || !features.outputSchema || (features.outputSchema?.length || 0) === 0 ) throw new CustomHttpError(features.message, HttpStatusCode.NOT_FOUND);

      const featureListEndResult: FeatureListEndResult = {
        featureList: features.outputSchema,
        totalRecords
      };
      const featureListResponse: FeatureListResponse = {
        ...initiateFeatureListResponse,
        state: true,
        message:  "Ok.",
        outputSchema: featureListEndResult
      };
      res.status(HttpStatusCode.OK).send(featureListResponse);
    } catch (error: any) {
      const featureListResponse: FeatureListResponse = {
        ...initiateFeatureListResponse,
        message: error?.message || "Internal server error.",
      };
      const statusCode = AppHelper.isHttpError(error)? error.statusCode : HttpStatusCode.INTERNAL_SERVER_ERROR;
      res.status(statusCode).send(featureListResponse);
    }
  }

  public async addFeature(req: Request, res: Response): Promise<void> {
    const dbTransaction: Transaction = await sequelize.transaction();
    try {
      const request: FeatureAddRequest = { ...req.body || {}} as FeatureAddRequest
      if (
        [
          request?.featureName,
          request?.reading,
          request?.creating,
          request?.updating,
          request?.deleting,
          request?.activating,
          request?.inactivating,
          request?.importing,
          request?.exporting,
          request?.approving,
          request?.rejecting,
          request?.special,
        ].some(AppHelper.isHasNoValue)
      ) {
        throw new CustomHttpError(
          "Bad Request: featureName, reading, creating, updating, deleting, activating, inactivating, importing, exporting, approving, rejecting, special as body is required.",
          HttpStatusCode.BAD_REQUEST
        );
      }

      const featureAdd: BaseResponse<string> = await this.featureService.addFeature(
        request,
        dbTransaction
      );
      if(!featureAdd.state || !featureAdd.outputSchema) throw new CustomHttpError(featureAdd.message, HttpStatusCode.BAD_REQUEST);

      const featureAddEndResult: FeatureAddEndResult = { featureCode: featureAdd.outputSchema };

      const featureAddResponse: FeatureAddResponse = {
        ...initiateFeatureAddResponse,
        state: true,
        message: `New feature added.`,
        outputSchema: featureAddEndResult,
      };
      await dbTransaction.commit();
      res.status(HttpStatusCode.CREATED).send(featureAddResponse);
    } catch(error: any) {
      await dbTransaction.rollback();
      const featureAddResponse: FeatureAddResponse = {
        ...initiateFeatureAddResponse,
        message: error?.message || "Internal server error.",
      };
      const statusCode = AppHelper.isHttpError(error)? error.statusCode : HttpStatusCode.INTERNAL_SERVER_ERROR;
      res.status(statusCode).send(featureAddResponse);
    }
  }

  public async editFeature(req: Request, res: Response): Promise<void> {
    const dbTransaction: Transaction = await sequelize.transaction();
    try {
      const request: FeatureEditRequest = { ...req.body || {}} as FeatureEditRequest
      if (
        [
          request?.featureCode,
          request?.featureName,
          request?.reading,
          request?.creating,
          request?.updating,
          request?.deleting,
          request?.activating,
          request?.inactivating,
          request?.importing,
          request?.exporting,
          request?.approving,
          request?.rejecting,
          request?.special,
        ].some(AppHelper.isHasNoValue)
      ) {
        throw new CustomHttpError(
          "Bad Request: featureCode, featureName, reading, creating, updating, deleting, activating, inactivating, importing, exporting, approving, rejecting, special as body is required.",
          HttpStatusCode.BAD_REQUEST
        );
      }

      const featureEdit: BaseResponse<string> = await this.featureService.editFeature(
        request,
        dbTransaction
      );

      if(!featureEdit.state || !featureEdit.outputSchema) throw new CustomHttpError(featureEdit.message, HttpStatusCode.BAD_REQUEST);

      const featureEditEndResult: FeatureEditEndResult = { featureCode: featureEdit.outputSchema };

      const featureEditResponse: FeatureEditResponse = {
        ...initiateFeatureAddResponse,
        state: true,
        message: `Feature updated`,
        outputSchema: featureEditEndResult,
      };
      await dbTransaction.commit();
      res.status(HttpStatusCode.OK).send(featureEditResponse);
    } catch(error: any) {
      await dbTransaction.rollback();
      const featureAddResponse: FeatureAddResponse = {
        ...initiateFeatureAddResponse,
        message: error?.message || "Internal server error.",
      };
      const statusCode = AppHelper.isHttpError(error)? error.statusCode : HttpStatusCode.INTERNAL_SERVER_ERROR;
      res.status(statusCode).send(featureAddResponse);
    }
  }

  public async deleteFeature(req: Request, res: Response): Promise<void> {
    const dbTransaction: Transaction = await sequelize.transaction();
    try {
      const request: FeatureDeleteRequest = { ...req.body || {}} as FeatureDeleteRequest
      if (AppHelper.isHasNoValue(request?.featureCode)) {
        throw new CustomHttpError(
          "Bad Request: featureCode as body is required.",
          HttpStatusCode.BAD_REQUEST
        );
      }

      const featureDelete: BaseResponse<string> = await this.featureService.deleteFeature(
        request,
        dbTransaction
      );

      if(!featureDelete.state || !featureDelete.outputSchema) throw new CustomHttpError(featureDelete.message, HttpStatusCode.BAD_REQUEST);

      const featureDeleteEndResult: FeatureDeleteEndResult = { featureCode: featureDelete.outputSchema };

      const featureDeleteResponse: FeatureDeleteResponse = {
        ...initiateFeatureAddResponse,
        state: true,
        message: `Feature deleted`,
        outputSchema: featureDeleteEndResult,
      };
      await dbTransaction.commit();
      res.status(HttpStatusCode.OK).send(featureDeleteResponse);
    } catch(error: any) {
      await dbTransaction.rollback();
      const featureAddResponse: FeatureAddResponse = {
        ...initiateFeatureAddResponse,
        message: error?.message || "Internal server error.",
      };
      const statusCode = AppHelper.isHttpError(error)? error.statusCode : HttpStatusCode.INTERNAL_SERVER_ERROR;
      res.status(statusCode).send(featureAddResponse);
    }
  }

  
}

export default FeatureController;
