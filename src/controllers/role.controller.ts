import { Request, Response } from "express";
import { BaseResponse } from "../dto/base.dto";
import { FeatureList } from "../dto/feature/feature-list.dto";
import { RoleDetail, RoleDetailEndResult, RoleDetailRequest, RoleDetailResponse, initiateRoleDetailResponse } from "../dto/role/role-detail.dto";
import { RoleList, RoleListEndResult, RoleListRequest, RoleListResponse, initiateRoleListResponse } from "../dto/role/role-list.dto";
import RoleService from "../services/role.service";
import { HttpStatusCode } from "../utils/app.const";
import CustomHttpError from "../utils/custom-httperror";
import AppHelper from "../utils/helpers";
import sequelize from "../config/db-config";
import { Transaction } from "sequelize";
import { RoleAddEndResult, RoleAddFeatureAccess, RoleAddRequest, RoleAddResponse, initiateRoleAddResponse } from "../dto/role/role-add.dto";
import { FeatureProps } from "../dto/feature/feature-detail.dto";
import { RoleEditEndResult, RoleEditFeatureAccess, RoleEditRequest, RoleEditResponse, initiateRoleEditResponse } from "../dto/role/role-edit.dto";
import { RoleDeleteEndResult, RoleDeleteRequest, RoleDeleteResponse, initiateRoleDeleteResponse } from "../dto/role/role-delete.dto";

class RoleController {
  private roleService: RoleService;

  constructor() {
    this.roleService = new RoleService();
  }

  private isFeatureValid (feature: RoleAddFeatureAccess | RoleEditFeatureAccess, featureProps: string[]): boolean {
    for (const prop of featureProps) {
      if (!(prop in feature)) {
        return false;
      }
    }
    return true;
  }

  public async getRoleList(req: Request, res: Response): Promise<void> {
    try {
      const request: RoleListRequest = { ...req.query || {}} as unknown as RoleListRequest;
      if (
        request?.search == null ||
        [request?.page, request?.perPage].some(AppHelper.isHasNoValue)
      ) {
        throw new CustomHttpError(
          "Bad Request: search, page and perPage query string is required.",
          HttpStatusCode.BAD_REQUEST
        );
      }

      const roles: BaseResponse<RoleList[]> = await this.roleService.getRoleList(request);
      const totalRecords: number = await this.roleService.getRoleListTotalRecords(request.search) ?? 0;
      
      if(!roles.state || !roles.outputSchema || (roles.outputSchema?.length || 0) === 0 ) throw new CustomHttpError(roles.message, HttpStatusCode.NOT_FOUND);
      
      const userListEndResult: RoleListEndResult = {
        roleList: roles.outputSchema,
        totalRecords
      };
      const userListResponse: RoleListResponse = {
        ...initiateRoleListResponse,
        state: true,
        message:  roles.message,
        outputSchema: userListEndResult
      };
      res.status(HttpStatusCode.OK).send(userListResponse);
    } catch (error: any) {
      const userListResponse: RoleListResponse = {
        ...initiateRoleListResponse,
        message: error?.message || "Internal server error.",
      };
      const statusCode = AppHelper.isHttpError(error)? error.statusCode : HttpStatusCode.INTERNAL_SERVER_ERROR;
      res.status(statusCode).send(userListResponse);
    }
  }

  public async getRoleDetail(req: Request, res: Response): Promise<void> {
    try {
      const request: RoleDetailRequest = { ...req.query || {}} as unknown as RoleDetailRequest;
      if (AppHelper.isHasNoValue(request?.roleCode)) {
        throw new CustomHttpError(
          "Bad Request: roleCode query string is required.",
          HttpStatusCode.BAD_REQUEST
        );
      }

      const role: BaseResponse<RoleDetail> = await this.roleService.getRoleDetail(request);
      
      if(!role.state || !role?.outputSchema) throw new CustomHttpError(role.message, HttpStatusCode.NOT_FOUND);

      const featureList: BaseResponse<FeatureList[]> = await this.roleService.getRoleFeatureAccessList(role.outputSchema.roleCode);

      const userDetailEndResult: RoleDetailEndResult = {
        roleDetail: role.outputSchema, featureList: featureList.outputSchema || []
      };
      const userDetailResponse: RoleDetailResponse = {
        ...initiateRoleDetailResponse,
        state: true,
        message: role.message,
        outputSchema: userDetailEndResult
      };
      res.status(HttpStatusCode.OK).send(userDetailResponse);
    } catch (error: any) {
      const userDetailResponse: RoleDetailResponse = {
        ...initiateRoleDetailResponse,
        message: error?.message || "Internal server error.",
      };
      const statusCode = AppHelper.isHttpError(error)? error.statusCode : HttpStatusCode.INTERNAL_SERVER_ERROR;
      res.status(statusCode).send(userDetailResponse);
    }
  }

  public async addRole(req: Request, res: Response): Promise<void> {
    const dbTransaction: Transaction = await sequelize.transaction();
    try {
      const request: RoleAddRequest = { ...req.body || {}} as RoleAddRequest
      if (
        [
          request?.roleName,
          request?.authDirectAddress
        ].some(AppHelper.isHasNoValue),
        (request?.featureList?.length || 0) === 0
      ) {
        throw new CustomHttpError("Bad Request: roleName, authDirectAddress and featureList as body is required.", HttpStatusCode.BAD_REQUEST);
      }

      const featureProps = ["featureCode", ...FeatureProps];
      const FeaturePropsValidate = (AppHelper.cloneDeep(request.featureList)! as RoleAddFeatureAccess[]).every(x=> this.isFeatureValid(x, featureProps));
      if(!FeaturePropsValidate){
        throw new CustomHttpError(`Bad Request: Feature list props ${featureProps.join(', ')} as body is required.`, HttpStatusCode.BAD_REQUEST);
      }

      const roleAdd: BaseResponse<string> = await this.roleService.addRole(
        request,
        dbTransaction
      );

      if(!roleAdd.state || !roleAdd.outputSchema) throw new CustomHttpError(roleAdd.message, HttpStatusCode.BAD_REQUEST);

      const roleAddEndResult: RoleAddEndResult = { roleCode: roleAdd.outputSchema};

      const roleAddResponse: RoleAddResponse = {
        ...initiateRoleAddResponse,
        state: true,
        message: roleAdd.message,
        outputSchema: roleAddEndResult,
      };
      await dbTransaction.commit();
      res.status(HttpStatusCode.CREATED).send(roleAddResponse);
    } catch(error: any) {
      await dbTransaction.rollback();
      const roleAddResponse: RoleAddResponse = {
        ...initiateRoleAddResponse,
        message: error?.message || "Internal server error.",
      };
      const statusCode = AppHelper.isHttpError(error)? error.statusCode : HttpStatusCode.INTERNAL_SERVER_ERROR;
      res.status(statusCode).send(roleAddResponse);
    }
  }

  public async editRole(req: Request, res: Response): Promise<void> {
    const dbTransaction: Transaction = await sequelize.transaction();
    try {
      const request: RoleEditRequest = { ...req.body || {}} as RoleEditRequest
      if (
        [
          request?.roleCode, 
          request?.roleName, 
          request?.authDirectAddress
        ].some(AppHelper.isHasNoValue)
      ) {
        throw new CustomHttpError("Bad Request: roleCode, roleName, authDirectAddress as body is required.", HttpStatusCode.BAD_REQUEST);
      }

      if((request?.featureList?.length || 0) > 0){
        const featureProps = ["featureCode", ...FeatureProps];
        const FeaturePropsValidate = (AppHelper.cloneDeep(request.featureList)! as RoleAddFeatureAccess[]).every(x=> this.isFeatureValid(x, featureProps));
        if(!FeaturePropsValidate){
          throw new CustomHttpError(`Bad Request: Feature list props ${featureProps.join(', ')} as body is required.`, HttpStatusCode.BAD_REQUEST);
        }
      }

      const roleEdit: BaseResponse<string> = await this.roleService.EditRole(
        request,
        dbTransaction
      );

      if(!roleEdit.state || !roleEdit.outputSchema) throw new CustomHttpError(roleEdit.message, HttpStatusCode.BAD_REQUEST);

      const featureEditEndResult: RoleEditEndResult = { roleCode: roleEdit.outputSchema};

      const roleEditResponse: RoleEditResponse = {
        ...initiateRoleEditResponse,
        state: true,
        message: roleEdit.message,
        outputSchema: featureEditEndResult,
      };
      await dbTransaction.commit();
      res.status(HttpStatusCode.OK).send(roleEditResponse);
    } catch(error: any) {
      await dbTransaction.rollback();
      const roleEditResponse: RoleEditResponse = {
        ...initiateRoleEditResponse,
        message: error?.message || "Internal server error.",
      };
      const statusCode = AppHelper.isHttpError(error)? error.statusCode : HttpStatusCode.INTERNAL_SERVER_ERROR;
      res.status(statusCode).send(roleEditResponse);
    }
  }

  public async deleteRole(req: Request, res: Response): Promise<void> {
    const dbTransaction: Transaction = await sequelize.transaction();
    try {
      const request: RoleDeleteRequest = { ...req.body || {}} as RoleDeleteRequest
      if (AppHelper.isHasNoValue(request?.roleCode)) {
        throw new CustomHttpError("Bad Request: roleCode as body is required.", HttpStatusCode.BAD_REQUEST);
      }

      const roleDelete: BaseResponse<string> = await this.roleService.deleteRole(
        request,
        dbTransaction
      );

      if(!roleDelete.state || !roleDelete.outputSchema) throw new CustomHttpError(roleDelete.message, HttpStatusCode.BAD_REQUEST);

      const featureDeleteEndResult: RoleDeleteEndResult = { roleCode: roleDelete.outputSchema};

      const roleDeleteResponse: RoleDeleteResponse = {
        ...initiateRoleDeleteResponse,
        state: true,
        message: roleDelete.message,
        outputSchema: featureDeleteEndResult,
      };
      await dbTransaction.commit();
      res.status(HttpStatusCode.OK).send(roleDeleteResponse);
    } catch(error: any) {
      await dbTransaction.rollback();
      const roleDeleteResponse: RoleDeleteResponse = {
        ...initiateRoleDeleteResponse,
        message: error?.message || "Internal server error.",
      };
      const statusCode = AppHelper.isHttpError(error)? error.statusCode : HttpStatusCode.INTERNAL_SERVER_ERROR;
      res.status(statusCode).send(roleDeleteResponse);
    }
  }
  
}

export default RoleController;
