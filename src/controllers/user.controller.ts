import { Request, Response } from "express";
import { FeatureList } from "../dto/feature/feature-list.dto";
import {
  UserDetail,
  UserDetailEndResult,
  UserDetailRequest,
  UserDetailResponse,
  initiateUserDetailResponse,
} from "../dto/user/user-detail.dto";
import {
  UserList,
  UserListEndResult,
  UserListRequest,
  UserListResponse,
  initiateUserListResponse,
} from "../dto/user/user-list.dto";
import RoleService from "../services/role.service";
import UserService from "../services/user.service";
import { HttpStatusCode } from "../utils/app.const";
import CustomHttpError from "../utils/custom-httperror";
import AppHelper from "../utils/helpers";
import { BaseResponse } from "../dto/base.dto";
import { Transaction } from "sequelize";
import sequelize from "../config/db-config";
import { UserAddEndResult, UserAddRequest, UserAddResponse, initiateUserAddResponse, validateUserAddRequest } from "../dto/user/user-add.dto";
import { UserEditEndResult, UserEditRequest, UserEditResponse, initiateUserEditResponse, validateUserEditRequest } from "../dto/user/user-edit.dto";
import { UserDeleteEndResult, UserDeleteRequest, UserDeleteResponse, initiateUserDeleteResponse } from "../dto/user/user-delete.dto";


class UserController {
  private userService: UserService;
  private roleService: RoleService;

  constructor() {
    this.userService = new UserService();
    this.roleService = new RoleService();
  }

  public async getUserList(req: Request, res: Response): Promise<void> {
    try {
      const request: UserListRequest = { ...req.query || {}} as unknown as UserListRequest;
      if (
        request?.search == null ||
        [request?.page, request?.perPage].some(AppHelper.isHasNoValue)
      ) {
        throw new CustomHttpError(
          "Bad Request: search, page and perPage query string is required.",
          HttpStatusCode.BAD_REQUEST
        );
      }

      const users: BaseResponse<UserList[]> = await this.userService.getUserList(request);
      const totalRecords: number = await this.userService.getUserListTotalRecords(request.search) ?? 0;
      
      if(!users.state || !users.outputSchema || (users.outputSchema?.length || 0) === 0 ) throw new CustomHttpError(users.message, HttpStatusCode.NOT_FOUND);
      
      const userListEndResult: UserListEndResult = {
        userList: users.outputSchema,
        totalRecords
      };
      const userListResponse: UserListResponse = {
        ...initiateUserListResponse,
        state: true,
        message:  "Ok.",
        outputSchema: userListEndResult
      };
      res.status(HttpStatusCode.OK).send(userListResponse);
    } catch (error: any) {
      const userListResponse: UserListResponse = {
        ...initiateUserListResponse,
        message: error?.message || "Internal server error.",
      };
      const statusCode = AppHelper.isHttpError(error)? error.statusCode : HttpStatusCode.INTERNAL_SERVER_ERROR;
      res.status(statusCode).send(userListResponse);
    }
  }

  public async getUserDetail(req: Request, res: Response): Promise<void> {
    try {
      const request: UserDetailRequest = { ...req.query || {}} as unknown as UserDetailRequest;
      if (AppHelper.isHasNoValue(request?.userCode)) {
        throw new CustomHttpError(
          "Bad Request: userCode query string is required.",
          HttpStatusCode.BAD_REQUEST
        );
      }

      const user: BaseResponse<UserDetail> | null = await this.userService.getUserDetail(request);
      
      if(!user.state || !user?.outputSchema) throw new CustomHttpError(user.message, HttpStatusCode.NOT_FOUND);

      const featureList: BaseResponse<FeatureList[]> = await this.roleService.getRoleFeatureAccessList(user.outputSchema.roleCode);
      const userDetailEndResult: UserDetailEndResult = {
        userDetail: user.outputSchema, featureList: featureList.outputSchema || []
      };
      const userDetailResponse: UserDetailResponse = {
        ...initiateUserDetailResponse,
        state: true,
        message: "Ok.",
        outputSchema: userDetailEndResult
      };
      res.status(HttpStatusCode.OK).send(userDetailResponse);
    } catch (error: any) {
      const userDetailResponse: UserDetailResponse = {
        ...initiateUserDetailResponse,
        message: error?.message || "Internal server error.",
      };
      const statusCode = AppHelper.isHttpError(error)? error.statusCode : HttpStatusCode.INTERNAL_SERVER_ERROR;
      res.status(statusCode).send(userDetailResponse);
    }
  }

  public async addUser(req: Request, res: Response): Promise<void> {
    const dbTransaction: Transaction = await sequelize.transaction();
    try {
      const request: UserAddRequest = { ...req.body || {}} as UserAddRequest
      
      const validateMsg = validateUserAddRequest(request);
      if (validateMsg) throw new CustomHttpError(validateMsg, HttpStatusCode.BAD_REQUEST);

      const userAdd: BaseResponse<string> = await this.userService.addUser(
        request,
        dbTransaction
      );

      if(!userAdd.state || !userAdd.outputSchema) throw new CustomHttpError(userAdd.message, HttpStatusCode.BAD_REQUEST);

      const userAddEndResult: UserAddEndResult = { userCode: userAdd.outputSchema };

      const userAddResponse: UserAddResponse = {
        ...initiateUserAddResponse,
        state: true,
        message: userAdd.message,
        outputSchema: userAddEndResult,
      };
      await dbTransaction.commit();
      res.status(HttpStatusCode.CREATED).send(userAddResponse);
    } catch(error: any) {
      await dbTransaction.rollback();
      const roleAddResponse: UserAddResponse = {
        ...initiateUserAddResponse,
        message: error?.message || "Internal server error.",
      };
      const statusCode = AppHelper.isHttpError(error)? error.statusCode : HttpStatusCode.INTERNAL_SERVER_ERROR;
      res.status(statusCode).send(roleAddResponse);
    }
  }

  public async editUser(req: Request, res: Response): Promise<void> {
    const dbTransaction: Transaction = await sequelize.transaction();
    try {
      const request: UserEditRequest = { ...req.body || {}} as UserEditRequest
      
      const validateMsg = validateUserEditRequest(request);
      if (validateMsg) throw new CustomHttpError(validateMsg, HttpStatusCode.BAD_REQUEST);

      const userEdit: BaseResponse<string> = await this.userService.editUser(
        request,
        dbTransaction
      );

      if(!userEdit.state || !userEdit.outputSchema) throw new CustomHttpError(userEdit.message, HttpStatusCode.BAD_REQUEST);

      const userEditEndResult: UserEditEndResult = { userCode: userEdit.outputSchema };

      const userEditResponse: UserEditResponse = {
        ...initiateUserEditResponse,
        state: true,
        message: userEdit.message,
        outputSchema: userEditEndResult,
      };
      await dbTransaction.commit();
      res.status(HttpStatusCode.OK).send(userEditResponse);
    } catch(error: any) {
      await dbTransaction.rollback();
      const roleEditResponse: UserEditResponse = {
        ...initiateUserEditResponse,
        message: error?.message || "Internal server error.",
      };
      const statusCode = AppHelper.isHttpError(error)? error.statusCode : HttpStatusCode.INTERNAL_SERVER_ERROR;
      res.status(statusCode).send(roleEditResponse);
    }
  }

  public async deleteUser(req: Request, res: Response): Promise<void> {
    const dbTransaction: Transaction = await sequelize.transaction();
    try {
      const request: UserDeleteRequest = { ...req.body || {}} as UserDeleteRequest
      if (AppHelper.isHasNoValue(request?.userCode)) {
        throw new CustomHttpError("Bad Request: userCode as body is required.", HttpStatusCode.BAD_REQUEST);
      }

      const userDelete: BaseResponse<string> = await this.userService.deleteUser(
        request,
        dbTransaction
      );

      if(!userDelete.state || !userDelete.outputSchema) throw new CustomHttpError(userDelete.message, HttpStatusCode.BAD_REQUEST);

      const userDeleteEndResult: UserDeleteEndResult = { userCode: userDelete.outputSchema };

      const userDeleteResponse: UserDeleteResponse = {
        ...initiateUserDeleteResponse,
        state: true,
        message: userDelete.message,
        outputSchema: userDeleteEndResult,
      };
      await dbTransaction.commit();
      res.status(HttpStatusCode.OK).send(userDeleteResponse);
    } catch(error: any) {
      await dbTransaction.rollback();
      const roleDeleteResponse: UserDeleteResponse = {
        ...initiateUserDeleteResponse,
        message: error?.message || "Internal server error.",
      };
      const statusCode = AppHelper.isHttpError(error)? error.statusCode : HttpStatusCode.INTERNAL_SERVER_ERROR;
      res.status(statusCode).send(roleDeleteResponse);
    }
  }

}

export default UserController;
