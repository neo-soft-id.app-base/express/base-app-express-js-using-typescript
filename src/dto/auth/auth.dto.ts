import { BaseResponse } from "../base.dto";
import { UserDetailByAuth } from "../user/user-detail-by-auth.dto";

export interface AuthSignIn {
  token: AuthToken;
  user: UserDetailByAuth;
  permission?: string;
}
export interface AuthToken {
  type: string;
  key: string;
}

export interface AuthSignInResponse extends BaseResponse<AuthSignIn> {}

export const initiateAuthSignInResponse: AuthSignInResponse = {
  state: false,
  message: "",
  outputSchema: null
};
