export interface BaseResponse<T> {
  state: boolean;
  message: string;
  outputSchema?: T | null;
}
