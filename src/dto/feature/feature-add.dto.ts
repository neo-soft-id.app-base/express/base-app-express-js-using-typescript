import { BaseResponse } from "../base.dto";

export interface FeatureAddRequest {
  featureName: string;
  reading: boolean;
  creating: boolean;
  updating: boolean;
  deleting: boolean;
  activating: boolean;
  inactivating: boolean;
  importing: boolean;
  exporting: boolean;
  approving: boolean;
  rejecting: boolean;
  special: boolean;
}

export interface FeatureAddEndResult {
  featureCode: string | null;
}

export interface FeatureAddResponse extends BaseResponse<FeatureAddEndResult> { }

export const initiateFeatureAddResponse: FeatureAddResponse = {
  state: false,
  message: "",
  outputSchema: null
};
