import { BaseResponse } from "../base.dto";

export interface FeatureDeleteRequest {
  featureCode: string;
}

export interface FeatureDeleteEndResult {
  featureCode: string | null;
}

export interface FeatureDeleteResponse extends BaseResponse<FeatureDeleteEndResult> { }

export const initiateFeatureDeleteResponse: FeatureDeleteResponse = {
  state: false,
  message: "",
  outputSchema: null
};
