export const FeatureProps: string[] = [
  "reading",
  "creating",
  "updating",
  "deleting",
  "activating",
  "inactivating",
  "importing",
  "exporting",
  "approving",
  "rejecting",
  "special",
];
