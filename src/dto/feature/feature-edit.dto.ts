import { BaseResponse } from "../base.dto";

export interface FeatureEditRequest {
  featureCode: string;
  featureName: string;
  reading: boolean;
  creating: boolean;
  updating: boolean;
  deleting: boolean;
  activating: boolean;
  inactivating: boolean;
  importing: boolean;
  exporting: boolean;
  approving: boolean;
  rejecting: boolean;
  special: boolean;
}

export interface FeatureEditEndResult {
  featureCode: string | null;
}

export interface FeatureEditResponse extends BaseResponse<FeatureEditEndResult> { }

export const initiateFeatureEditResponse: FeatureEditResponse = {
  state: false,
  message: "",
  outputSchema: null
};
