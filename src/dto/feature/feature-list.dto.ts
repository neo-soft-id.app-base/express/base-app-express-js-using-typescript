import { BaseResponse } from "../base.dto";

export interface FeatureListRequest {
  page: string;
  perPage: string;
  search: string;
}

export interface FeatureList {
  featureCode: string;
  featureName: string;
  reading: boolean;
  creating: boolean;
  updating: boolean;
  deleting: boolean;
  activating: boolean;
  inactivating: boolean;
  importing: boolean;
  exporting: boolean;
  approving: boolean;
  rejecting: boolean;
  special: boolean;
}

export interface FeatureListEndResult {
  featureList: FeatureList[] | null
  totalRecords: number;
}

export interface FeatureListResponse extends BaseResponse<FeatureListEndResult> { }

export const initiateFeatureListResponse: FeatureListResponse = {
  state: false,
  message: "",
  outputSchema: null
};
