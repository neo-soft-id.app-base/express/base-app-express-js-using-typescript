import { BaseResponse } from "../base.dto";

export interface RoleAddFeatureAccess {
  featureCode: string;
  reading: boolean;
  creating: boolean;
  updating: boolean;
  deleting: boolean;
  activating: boolean;
  inactivating: boolean;
  importing: boolean;
  exporting: boolean;
  approving: boolean;
  rejecting: boolean;
  special: boolean;
}

export interface RoleAddRequest {
  roleName: string;
  authDirectAddress: string;
  featureList: RoleAddFeatureAccess[]
}

export interface RoleAddEndResult {
  roleCode: string | null;
}

export interface RoleAddResponse extends BaseResponse<RoleAddEndResult> { }

export const initiateRoleAddResponse: RoleAddResponse = {
  state: false,
  message: "",
  outputSchema: null
};
