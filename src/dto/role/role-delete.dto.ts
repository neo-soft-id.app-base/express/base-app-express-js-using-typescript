import { BaseResponse } from "../base.dto";

export interface RoleDeleteRequest {
  roleCode: string;
}

export interface RoleDeleteEndResult {
  roleCode: string | null;
}

export interface RoleDeleteResponse extends BaseResponse<RoleDeleteEndResult> { }

export const initiateRoleDeleteResponse: RoleDeleteResponse = {
  state: false,
  message: "",
  outputSchema: null
};
