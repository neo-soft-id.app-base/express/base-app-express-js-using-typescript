import { BaseResponse } from "../base.dto";
import { FeatureList } from "../feature/feature-list.dto";

export interface RoleDetailRequest {
  roleCode: string;
}

export interface RoleDetail {
  roleCode: string;
  roleName: string;
  authDirectAddress: string;
}

export interface RoleDetailEndResult {
  roleDetail: RoleDetail | null;
  featureList: FeatureList[] | null;
}

export interface RoleDetailResponse extends BaseResponse<RoleDetailEndResult> {}

export const initiateRoleDetailResponse: RoleDetailResponse = {
  state: false,
  message: "",
  outputSchema: null,
};
