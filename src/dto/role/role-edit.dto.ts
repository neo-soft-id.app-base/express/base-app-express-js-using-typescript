import { BaseResponse } from "../base.dto";

export interface RoleEditFeatureAccess {
  featureCode: string;
  reading: boolean;
  creating: boolean;
  updating: boolean;
  deleting: boolean;
  activating: boolean;
  inactivating: boolean;
  importing: boolean;
  exporting: boolean;
  approving: boolean;
  rejecting: boolean;
  special: boolean;
}

export interface RoleEditRequest {
  roleCode: string;
  roleName: string;
  authDirectAddress: string;
  featureList?: RoleEditFeatureAccess[] | null
}

export interface RoleEditEndResult {
  roleCode: string | null;
}

export interface RoleEditResponse extends BaseResponse<RoleEditEndResult> { }

export const initiateRoleEditResponse: RoleEditResponse = {
  state: false,
  message: "",
  outputSchema: null
};
