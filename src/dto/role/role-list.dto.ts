import { BaseResponse } from "../base.dto";


export interface RoleListRequest {
  page: string;
  perPage: string;
  search: string;
}

export interface RoleList {
  roleCode: string;
  roleName: string;
  authDirectAddress: string;
}

export interface RoleListEndResult {
  roleList: RoleList[] | null
  totalRecords: number;
}

export interface RoleListResponse extends BaseResponse<RoleListEndResult> { }

export const initiateRoleListResponse: RoleListResponse = {
  state: false,
  message: "",
  outputSchema: null
};
