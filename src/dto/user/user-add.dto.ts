import AppHelper from "../../utils/helpers";
import { BaseResponse } from "../base.dto";

export interface UserAddRequest {
  firstName: string;
  lastName?: string;
  username: string;
  password: string;
  phoneNumber?: string;
  email: string;
  genderId: number;
  birthPlace?: string;
  birthDay?: string;
  address?: string;
  role: UserAddRole;
}

export interface UserAddRole {
  roleCode: string;
}

export interface UserAddEndResult {
  userCode: string | null;
}

export interface UserAddResponse extends BaseResponse<UserAddEndResult> {}

export const initiateUserAddResponse: UserAddResponse = {
  state: false,
  message: "",
  outputSchema: null,
};

export function validateUserAddRequest(request: UserAddRequest): string | null {
  if (
    [
      request?.firstName,
      request?.username,
      request?.password,
      request?.phoneNumber,
      request?.email,
      request?.genderId,
      request?.role?.roleCode,
    ].some(AppHelper.isHasNoValue)
  ) return "Please fill in all required fields.";

  const usernameRegex = /^[a-zA-Z0-9_\.]{3,}$/;
  if (!usernameRegex.test(request.username))
    return "Please enter a valid username. Username should consist of letters, numbers, '_', or '.'.";

  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (!emailRegex.test(request.email))
    return "Please enter a valid email address.";

  const phoneNumberRegex = /^\+(?:\d ?){6,14}\d$/;
  if (!phoneNumberRegex.test(request.phoneNumber!))
    return "Please enter a phone number with a valid format.";

  return null;
}
