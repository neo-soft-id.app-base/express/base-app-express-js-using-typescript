import { BaseResponse } from "../base.dto";

export interface UserDeleteRequest {
  userCode: string;
}

export interface UserDeleteEndResult {
  userCode: string | null;
}

export interface UserDeleteResponse extends BaseResponse<UserDeleteEndResult> {}

export const initiateUserDeleteResponse: UserDeleteResponse = {
  state: false,
  message: "",
  outputSchema: null,
};
