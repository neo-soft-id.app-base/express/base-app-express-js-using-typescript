import { BaseResponse } from "../base.dto";
import { FeatureList } from "../feature/feature-list.dto";

export interface UserDetailByAuthRequest {
  username: string;
  password: string;
}

export interface UserDetailByAuth {
  roleCode: string;
  roleName: string;
  authDirectAddress: string;
  userCode: string;
  firstName: string;
  lastName: string;
  username: string;
}

export interface UserDetailByAuthEndResult {
  userDetail: UserDetailByAuth | null;
  featureList: FeatureList[] | null;
}

export interface UserDetailByAuthResponse extends BaseResponse<UserDetailByAuthEndResult> {}

export const initiateUserDetailByAuthResponse: UserDetailByAuthResponse = {
  state: false,
  message: "",
  outputSchema: null,
};
