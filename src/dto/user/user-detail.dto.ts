import { BaseResponse } from "../base.dto";
import { FeatureList } from "../feature/feature-list.dto";

export interface UserDetailRequest {
  userCode: string;
}

export interface UserDetail {
  roleCode: string;
  roleName: string;
  userCode: string;
  firstName: string;
  lastName: string;
  username: string;
  phoneNumber: string;
  email: string;
  genderId: number;
  genderName: string;
  birthPlace: string;
  birthDay: string;
  address: string;
}

export interface UserDetailEndResult {
  userDetail: UserDetail | null;
  featureList: FeatureList[] | null;
}

export interface UserDetailResponse extends BaseResponse<UserDetailEndResult> {}

export const initiateUserDetailResponse: UserDetailResponse = {
  state: false,
  message: "",
  outputSchema: null,
};
