import AppHelper from "../../utils/helpers";
import { BaseResponse } from "../base.dto";

export interface UserEditRequest {
  userCode: string;
  firstName: string;
  lastName?: string;
  username: string;
  password: string;
  phoneNumber?: string;
  email: string;
  genderId: number;
  birthPlace?: string;
  birthDay?: string;
  address?: string;
  role?: UserEditRole;
}

export interface UserEditRole {
  roleCode: string;
}

export interface UserEditEndResult {
  userCode: string | null;
}

export interface UserEditResponse extends BaseResponse<UserEditEndResult> {}

export const initiateUserEditResponse: UserEditResponse = {
  state: false,
  message: "",
  outputSchema: null,
};



export function validateUserEditRequest(request: UserEditRequest): string | null {
  if(AppHelper.isHasNoValue(request?.userCode)) return "Please fill userCode, required filed cannot be empty.";

  if(request?.username){
    const usernameRegex = /^[a-zA-Z0-9_\.]{3,}$/;
    if (!usernameRegex.test(request.username))
      return "Please enter a valid username. Username should consist of letters, numbers, '_', or '.'.";
  }

  if(request?.email){
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(request.email))
      return "Please enter a valid email address.";
  }

  if(request?.phoneNumber){
  const phoneNumberRegex = /^\+(?:\d ?){6,14}\d$/;
  if (!phoneNumberRegex.test(request.phoneNumber!))
    return "Please enter a phone number with a valid format.";
  }

  return null;
}
