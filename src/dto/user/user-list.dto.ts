import { BaseResponse } from "../base.dto";


export interface UserListRequest {
  page: string;
  perPage: string;
  search: string;
}

export interface UserList {
  roleCode: string;
  roleName: string;
  userCode: string;
  firstName: string;
  lastName?: string;
  username: string;
  email: string;
}

export interface UserListEndResult {
  userList: UserList[] | null
  totalRecords: number;
}

export interface UserListResponse extends BaseResponse<UserListEndResult> { }

export const initiateUserListResponse: UserListResponse = {
  state: false,
  message: "",
  outputSchema: null
};
