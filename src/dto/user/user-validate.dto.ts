import AppHelper from "../../utils/helpers";
import { UserAddRequest, UserAddRole } from "./user-add.dto";
import { UserEditRequest, UserEditRole } from "./user-edit.dto";

export interface ValidateUserData {
  username?: string;
  phoneNumber?: string;
  email?: string;
  role?: UserAddRole | UserEditRole;
}

