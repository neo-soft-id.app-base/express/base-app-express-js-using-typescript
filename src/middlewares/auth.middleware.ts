import { Request, Response, NextFunction } from 'express';
import AuthService from '../services/auth.service';

const authService = new AuthService();

export class AuthMiddleware {
  public static authenticate(req: Request, res: Response, next: NextFunction): void | Response {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];

    if (token == null) return res.sendStatus(401);
    
    const user = authService.verifyToken(token);
    if (!user) return res.sendStatus(403);

    next();
  }
}
