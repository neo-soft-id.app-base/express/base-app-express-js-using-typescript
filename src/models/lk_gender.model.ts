import { Model, DataTypes } from "sequelize";
import sequelize from "../config/db-config";

class LkGender extends Model {
  public id!: number;
  public genderName!: string;
}

LkGender.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false
    },
    genderName: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: true
    }
  },
  {
    sequelize,
    tableName: "lk_gender",
    timestamps: false
  }
);

export default LkGender;
