import { Model, DataTypes } from "sequelize";
import sequelize from "../config/db-config";
import MsRole from "./ms_role.model";
import MsFeature from "./ms_feature.model";

class MapRoleFeatureAccess extends Model {
  public id!: number;
  public roleCode!: string;
  public featureCode!: string;
  public reading!: boolean;
  public creating!: boolean;
  public updating!: boolean;
  public deleting!: boolean;
  public activating!: boolean;
  public inactivating!: boolean;
  public importing!: boolean;
  public exporting!: boolean;
  public approving!: boolean;
  public rejecting!: boolean;
  public special!: boolean;
}

MapRoleFeatureAccess.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    roleCode: {
      type: DataTypes.STRING(35),
      allowNull: false,
    },
    featureCode: {
      type: DataTypes.STRING(35),
      allowNull: false,
    },
    reading: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    creating: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    updating: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    deleting: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    activating: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    inactivating: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    importing: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    exporting: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    approving: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    rejecting: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    special: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: "map_role_feature_access",
    timestamps: false
  }
);


MapRoleFeatureAccess.belongsTo(MsRole, { foreignKey: 'roleCode', targetKey: 'roleCode', as: 'role' });
MapRoleFeatureAccess.belongsTo(MsFeature, { foreignKey: 'featureCode', targetKey: 'featureCode', as: 'feature' });

export default MapRoleFeatureAccess;
