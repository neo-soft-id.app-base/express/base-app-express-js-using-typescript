import { Model, DataTypes } from "sequelize";
import sequelize from "../config/db-config";
import MsUser from "./ms_user.model";
import MsRole from "./ms_role.model";

class MapUserRole extends Model {
  public id!: number;
  public userCode!: string;
  public roleCode!: string;
}

MapUserRole.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    userCode: {
      type: DataTypes.STRING(35),
      allowNull: false
    },
    roleCode: {
      type: DataTypes.STRING(35),
      allowNull: false
    }
  },
  {
    sequelize,
    tableName: "map_user_role",
    timestamps: false
  }
);

MapUserRole.belongsTo(MsUser, { foreignKey: 'userCode', targetKey: 'userCode', as: 'user' });
MapUserRole.belongsTo(MsRole, { foreignKey: 'roleCode', targetKey: 'roleCode', as: 'role' });


export default MapUserRole;
