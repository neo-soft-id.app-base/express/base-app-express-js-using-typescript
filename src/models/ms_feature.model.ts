import { DataTypes, Model } from "sequelize";
import sequelize from "../config/db-config";

class MsFeature extends Model {
  public id!: number;
  public featureCode!: string;
  public featureName!: string;
  public reading!: boolean;
  public creating!: boolean;
  public updating!: boolean;
  public deleting!: boolean;
  public activating!: boolean;
  public inactivating!: boolean;
  public importing!: boolean;
  public exporting!: boolean;
  public approving!: boolean;
  public rejecting!: boolean;
  public special!: boolean;
  public isActive!: boolean;
}

MsFeature.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    featureCode: {
      type: DataTypes.STRING(35),
      allowNull: false,
      unique: true
    },
    featureName: {
      type: DataTypes.STRING(30),
      allowNull: false,
    },
    reading: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    creating: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    updating: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    deleting: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    activating: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    inactivating: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    importing: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    exporting: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    approving: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    rejecting: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    special: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
  },
  {
    sequelize,
    tableName: "ms_feature",
    timestamps: false,
  }
);

export default MsFeature;
