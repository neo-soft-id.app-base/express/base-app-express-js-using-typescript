import { DataTypes, Model } from "sequelize";
import sequelize from "../config/db-config";

class MsRole extends Model {
  public id!: number;
  public roleCode!: string;
  public roleName!: string;
  public isActive!: boolean;
}

MsRole.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    roleCode: {
      type: DataTypes.STRING(35),
      allowNull: false,
      unique: true
    },
    roleName: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    authDirectAddress: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
  },
  {
    sequelize,
    tableName: "ms_role",
    timestamps: false
  }
);

export default MsRole;
