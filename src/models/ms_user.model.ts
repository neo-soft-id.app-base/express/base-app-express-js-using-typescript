import { Model, DataTypes } from "sequelize";
import sequelize from "../config/db-config";
import LkGender from "./lk_gender.model";
import MapUserRole from "./map_user_role.model";

class MsUser extends Model {
  public id!: number;
  public userCode!: string;
  public firstName!: string;
  public lastName!: string | null;
  public username!: string;
  public password!: string;
  public phoneNumber!: string | null;
  public email!: string;
  public genderId!: boolean;
  public birthPlace!: string | null;
  public birthDay!: Date | null;
  public address!: string | null;
  public isActive!: boolean;
}

MsUser.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false
    },
    userCode: {
      type: DataTypes.STRING(35),
      allowNull: false,
      unique: true
    },
    firstName: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    lastName: {
      type: DataTypes.STRING(50),
      allowNull: true,
      defaultValue: null
    },
    username: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    password: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    phoneNumber: {
      type: DataTypes.STRING(20),
      allowNull: true,
      defaultValue: null
    },
    email: {
      type: DataTypes.STRING(40),
      allowNull: false
    },
    genderId: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    birthPlace: {
      type: DataTypes.STRING(100),
      allowNull: true,
      defaultValue: null
    },
    birthDay: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      defaultValue: null
    },
    address: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: null
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
  },
  {
    sequelize,
    tableName: "ms_user",
    timestamps: false
  }
);

MsUser.hasOne(LkGender, { foreignKey: 'id', sourceKey: 'genderId', as: 'gender' });

export default MsUser;
