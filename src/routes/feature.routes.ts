import { Router } from "express";
import FeatureController from "../controllers/feature.controller";
import { AuthMiddleware } from "../middlewares/auth.middleware";

const router = Router();
const featureController = new FeatureController();

router.get("/list", AuthMiddleware.authenticate, featureController.getFeatureList.bind(featureController));
router.post("/add", AuthMiddleware.authenticate, featureController.addFeature.bind(featureController));
router.post("/edit", AuthMiddleware.authenticate, featureController.editFeature.bind(featureController));
router.delete("/delete", AuthMiddleware.authenticate, featureController.deleteFeature.bind(featureController));

export default router;
