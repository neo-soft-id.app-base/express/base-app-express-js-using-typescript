import { Router } from "express";
import RoleController from "../controllers/role.controller";
import { AuthMiddleware } from "../middlewares/auth.middleware";

const router = Router();
const roleController = new RoleController();

router.get("/list", AuthMiddleware.authenticate, roleController.getRoleList.bind(roleController));
router.get("/detail", AuthMiddleware.authenticate, roleController.getRoleDetail.bind(roleController));
router.post("/add", AuthMiddleware.authenticate, roleController.addRole.bind(roleController));
router.post("/edit", AuthMiddleware.authenticate, roleController.editRole.bind(roleController));
router.delete("/delete", AuthMiddleware.authenticate, roleController.deleteRole.bind(roleController));

export default router;
