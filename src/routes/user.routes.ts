import { Router } from "express";
import { AuthMiddleware } from "../middlewares/auth.middleware";
import UserController from "../controllers/user.controller";

const router = Router();
const userController = new UserController();

router.get("/list", AuthMiddleware.authenticate, userController.getUserList.bind(userController));
router.get("/detail", AuthMiddleware.authenticate, userController.getUserDetail.bind(userController));
router.post("/add", AuthMiddleware.authenticate, userController.addUser.bind(userController));
router.post("/edit", AuthMiddleware.authenticate, userController.editUser.bind(userController));
router.delete("/delete", AuthMiddleware.authenticate, userController.deleteUser.bind(userController));

export default router;
