import jwt from "jsonwebtoken";
import MsUser from "../models/ms_user.model";
import AppHelper from "../utils/helpers";
import { UserDetailByAuth, UserDetailByAuthRequest } from "../dto/user/user-detail-by-auth.dto";
import { BaseResponse } from "../dto/base.dto";
import sequelize from "../config/db-config";
import MapUserRole from "../models/map_user_role.model";
import MsRole from "../models/ms_role.model";

const SECRET_KEY = "DFJDSF73MR43NFKUDHFH43RMBEWJVFUWGUEFNBYF76";

export default class AuthService {
  public async matchAuth(username: string, password: string): Promise<boolean> {
    const findUser = await MsUser.findOne({
      where: {
        username,
        password: AppHelper.md5(password),
        isActive: true
      },
    });

    if (!findUser) return false;

    return true;
  }

  public generateToken(username: string): string {
    return jwt.sign({ username }, SECRET_KEY, { expiresIn: "1h" });
  }

  public verifyToken(token: string): any {
    try {
      return jwt.verify(token, SECRET_KEY);
    } catch (err) {
      return null;
    }
  }

  public async getUserDetailByAuth(request: UserDetailByAuthRequest): Promise<BaseResponse<UserDetailByAuth>> {
    try {
      const userDetail = await MapUserRole.findOne({
        attributes: [
          [sequelize.col("role.roleCode"), "roleCode"],
          [sequelize.col("role.roleName"), "roleName"],
          [sequelize.col("role.authDirectAddress"), "authDirectAddress"],
          [sequelize.col("user.userCode"), "userCode"],
          [sequelize.col("user.firstName"), "firstName"],
          [sequelize.col("user.lastName"), "lastName"],
          [sequelize.col("user.username"), "username"]
        ],
        include: [
          {
            model: MsUser,
            as: "user",
            required: false,
            right: true,
            attributes: [],
          },
          {
            model: MsRole,
            as: "role",
            required: false,
            attributes: [],
          },
        ],
        where: { 
          '$user.username$': request.username,
          '$user.password$': AppHelper.md5(request.password), 
          '$user.isActive$': true 
        },      
        raw: true,
      });
      
      if (!userDetail) throw new Error(`User not found.`);

      const response: BaseResponse<UserDetailByAuth> = {
        state: true,
        message: "Ok.",
        outputSchema: AppHelper.cloneDeep(userDetail) as UserDetailByAuth,
      };
      return response;
    } catch (error: any) {
      return AppHelper.baseHandleError(error?.message);
    }
  }
}
