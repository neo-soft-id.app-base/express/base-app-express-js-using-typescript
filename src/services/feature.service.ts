import { Op, Transaction } from "sequelize";
import { FeatureList, FeatureListRequest } from "../dto/feature/feature-list.dto";
import MsFeature from "../models/ms_feature.model";
import AppHelper from "../utils/helpers";
import { FeatureAddRequest } from "../dto/feature/feature-add.dto";
import { FeatureEditRequest } from "../dto/feature/feature-edit.dto";
import MapRoleFeatureAccess from "../models/map_role_feature_access.model";
import { FeatureDeleteRequest } from "../dto/feature/feature-delete.dto";
import { BaseResponse } from "../dto/base.dto";
import { AppError } from "../utils/app.const";

export default class FeatureService {
  public async getFeatureList(request: FeatureListRequest): Promise<BaseResponse<FeatureList[]>> {
    try {
      const search: string = request.search;
      const endData: number = +request.perPage;
      const startData: number = +request.perPage * +request.page - +request.perPage;

      const featureList = await MsFeature.findAll({
        attributes: [
          "featureCode",
          "featureName",
          "reading",
          "creating",
          "updating",
          "deleting",
          "activating",
          "inactivating",
          "importing",
          "exporting",
          "approving",
          "rejecting",
          "special",
        ],
        where: {
          featureName: { [Op.like]: `%${search}%` },
          isActive: true
        },
        offset: startData,
        limit: endData,
        order: [
          ["featureName", "ASC"]
        ],
        raw: true,
      });

      if (!featureList || (featureList?.length || 0) === 0) throw new Error(`Features not found.`);

      featureList.map((x)=> {
        x.reading = !!x.reading
        x.creating = !!x.creating
        x.updating = !!x.updating
        x.deleting = !!x.deleting
        x.activating = !!x.activating
        x.inactivating = !!x.inactivating
        x.importing = !!x.importing
        x.exporting = !!x.exporting
        x.approving = !!x.approving
        x.rejecting = !!x.rejecting
        x.special = !!x.special
        return x;
      });

      const response: BaseResponse<FeatureList[]> = {
        state: true,
        message: "Ok.",
        outputSchema: AppHelper.cloneDeep(featureList) as FeatureList[],
      };
      return response;
    } catch (error: any) {
      return AppHelper.baseHandleError(error?.message);
    }
  }

  public async getFeatureListTotalRecords(search: string): Promise<number | null> {
    const totalRecords = await MsFeature.count({
      where: {
        featureName: { [Op.like]: `%${search}%` },
        isActive: true
      }
    });
    return totalRecords || 0;
  }

  public async addFeature(request: FeatureAddRequest, dbTransaction: Transaction): Promise<BaseResponse<string>> {
    try {
      const addMsFeature = await MsFeature.create(
        {
          featureCode: AppHelper.generateUniqueCode("FEAT"),
          featureName: request.featureName,
          reading: request.reading,
          creating: request.creating,
          updating: request.updating,
          deleting: request.deleting,
          activating: request.activating,
          inactivating: request.inactivating,
          importing: request.importing,
          exporting: request.exporting,
          approving: request.approving,
          rejecting: request.rejecting,
          special: request.special,
          isActive: true,
        },
        { transaction: dbTransaction }
      );

      if (!addMsFeature?.id) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Unable to Add new Feature`);

      const response: BaseResponse<string> = {
        state: true,
        message: "New Feature aded.",
        outputSchema: addMsFeature.featureCode
      };
      return response;
    } catch (error: any) {
      return AppHelper.baseHandleError(error?.message);
    }
  }

  public async editFeature(request: FeatureEditRequest, dbTransaction: Transaction): Promise<BaseResponse<string>> {
    try {
      const [updateMsFeature] = await MsFeature.update(
        {
          featureName: request.featureName,
          reading: request.reading,
          creating: request.creating,
          updating: request.updating,
          deleting: request.deleting,
          activating: request.activating,
          inactivating: request.inactivating,
          importing: request.importing,
          exporting: request.exporting,
          approving: request.approving,
          rejecting: request.rejecting,
          special: request.special,
        },
        {
          where: {
            featureCode: request.featureCode,
          },
          transaction: dbTransaction
        }
      );

      if (!updateMsFeature) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Unable to Update Feature`);

      const mapRoleFeatureAccesses: MapRoleFeatureAccess[] = await MapRoleFeatureAccess.findAll({
        where: {
          featureCode: request.featureCode,
        },
        order: [["id", "ASC"]],
        raw: true,
      });
      
      let updateMapRoleFeatureAccessStored: boolean[] = [];
      for(const mapRoleFeatureAccess of mapRoleFeatureAccesses){
        const updateMapRoleFeatureAccessData: Partial<MapRoleFeatureAccess> = {};
        for (const prop in request) {
          const requestData = request[prop as keyof FeatureEditRequest];
          const mapRoleFeatureAccessData = !!mapRoleFeatureAccess[prop as keyof MapRoleFeatureAccess];
          if (requestData) continue;
          if (mapRoleFeatureAccessData === requestData) continue;

          updateMapRoleFeatureAccessData[prop as keyof MapRoleFeatureAccess] = false;
        }
        if(Object.values(updateMapRoleFeatureAccessData).length === 0) continue;

        const [updateMapRoleFeatureAccess] = await MapRoleFeatureAccess.update(
          updateMapRoleFeatureAccessData,
          {
            where: {
              id: mapRoleFeatureAccess.id,
            },
            transaction: dbTransaction
          }
        );
        updateMapRoleFeatureAccessStored.push(!!updateMapRoleFeatureAccess);
      }
      
      if(updateMapRoleFeatureAccessStored.some(x=> x === false)) throw new Error(`${AppError.OOPS_SOMETHING_ISNT_QUITE_RIGHT} Unable to Add New Role Feature Access`);

      const response: BaseResponse<string> = {
        state: true,
        message: "Feature was edited.",
        outputSchema: request.featureCode,
      };
      return response;
    } catch (error: any) {
      return AppHelper.baseHandleError(error?.message);
    }
  }

  public async deleteFeature(request: FeatureDeleteRequest, dbTransaction: Transaction): Promise<BaseResponse<string>> {
    try {
      const [updateMsFeature] = await MsFeature.update(
        {
          isActive: false
        },
        {
          where: {
            featureCode: request.featureCode,
          },
          transaction: dbTransaction
        }
      );

      if (!updateMsFeature) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Unable to Remove Feature`);

      const deleteMapRoleFeatureAccess = await MapRoleFeatureAccess.destroy({
        where: {
          featureCode: request.featureCode,
        },
        transaction: dbTransaction,
      });

      if (!deleteMapRoleFeatureAccess) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Unable to Remove Role Feature Access`);

      const response: BaseResponse<string> = {
        state: true,
        message: "Feature was removed.",
        outputSchema: request.featureCode,
      };
      return response;

    } catch (error: any) {
      return AppHelper.baseHandleError(error?.message);
    }
  }
}
