import { Op, Transaction } from "sequelize";
import sequelize from "../config/db-config";
import { BaseResponse } from "../dto/base.dto";
import { FeatureProps } from "../dto/feature/feature-detail.dto";
import { FeatureList } from "../dto/feature/feature-list.dto";
import { RoleAddFeatureAccess, RoleAddRequest } from "../dto/role/role-add.dto";
import { RoleDeleteRequest } from "../dto/role/role-delete.dto";
import { RoleDetail, RoleDetailRequest } from "../dto/role/role-detail.dto";
import { RoleEditRequest } from "../dto/role/role-edit.dto";
import { RoleList, RoleListRequest } from "../dto/role/role-list.dto";
import MapRoleFeatureAccess from "../models/map_role_feature_access.model";
import MapUserRole from "../models/map_user_role.model";
import MsFeature from "../models/ms_feature.model";
import MsRole from "../models/ms_role.model";
import { AppError } from "../utils/app.const";
import AppHelper from "../utils/helpers";

export default class RoleService {
  public async getRoleList(request: RoleListRequest): Promise<BaseResponse<RoleList[]>> {
    try {
      const search: string = request.search;
      const endData: number = +request.perPage;
      const startData: number = +request.perPage * +request.page - +request.perPage;

      const roleList = await MsRole.findAll({
        attributes: ["roleCode", "roleName", "authDirectAddress"],
        where: {
          roleName: { [Op.like]: `%${search}%` },
          isActive: true,
        },
        offset: startData,
        limit: endData,
        order: [["roleName", "ASC"]],
        raw: true,
      });

      if (!roleList || (roleList?.length || 0) === 0) throw new Error(`Roles not found.`);

      const response: BaseResponse<RoleList[]> = {
        state: true,
        message: "Ok.",
        outputSchema: AppHelper.cloneDeep(roleList) as RoleList[],
      };
      return response;
    } catch (error: any) {
      return AppHelper.baseHandleError(error?.message);
    }
  }

  public async getRoleListTotalRecords(search: string): Promise<number> {
    const totalRecords = await MsRole.count({
      where: {
        roleName: { [Op.like]: `%${search}%` },
        isActive: true,
      },
    });
    return totalRecords || 0;
  }

  public async getRoleDetail(request: RoleDetailRequest): Promise<BaseResponse<RoleDetail>> {
    try {
      const roleDetail = await MsRole.findOne({
        attributes: ["roleCode", "roleName", "authDirectAddress"],
        where: { roleCode: request.roleCode, isActive: true },
        raw: true,
      });

      if (!roleDetail) throw new Error(`Role not found.`);

      const response: BaseResponse<RoleDetail> = {
        state: true,
        message: "Ok.",
        outputSchema: AppHelper.cloneDeep(roleDetail) as RoleDetail,
      };
      return response;
    } catch (error: any) {
      return AppHelper.baseHandleError(error?.message);
    }
  }

  public async getRoleFeatureAccessList(roleCode: string): Promise<BaseResponse<FeatureList[]>> {
    try {
      const results = await MapRoleFeatureAccess.findAll({
        attributes: [
          "featureCode",
          [sequelize.col("feature.featureName"), "featureName"],
          "reading",
          "creating",
          "updating",
          "deleting",
          "activating",
          "inactivating",
          "importing",
          "exporting",
          "approving",
          "rejecting",
          "special",
        ],
        include: [
          {
            model: MsRole,
            as: "role",
            attributes: [],
            required: true,
          },
          {
            model: MsFeature,
            as: "feature",
            attributes: [],
            required: true,
          },
        ],
        where: {
          roleCode,
        },
        order: [[sequelize.col("feature.featureName"), "ASC"]],
        raw: true,
      });
      
      if (!results || (results?.length || 0) === 0) throw new Error(`Role Feature Access not found.`);

      const resultMap = AppHelper.cloneDeep(results) as FeatureList[];
      if (resultMap) resultMap.map((x) => {
        x.reading = !!x.reading;
        x.creating = !!x.creating;
        x.updating = !!x.updating;
        x.deleting = !!x.deleting;
        x.activating = !!x.activating;
        x.inactivating = !!x.inactivating;
        x.importing = !!x.importing;
        x.exporting = !!x.exporting;
        x.approving = !!x.approving;
        x.rejecting = !!x.rejecting;
        x.special = !!x.special;
        return x;
      });

      const response: BaseResponse<FeatureList[]> = {
        state: true,
        message: "Ok.",
        outputSchema: resultMap
      };
      return response;
    } catch (error: any) {
      return AppHelper.baseHandleError(error?.message);
    }
  
  }

  public async addRole(request: RoleAddRequest, dbTransaction: Transaction): Promise<BaseResponse<string>> {
    try {
      const addMsRole = await MsRole.create(
        {
          roleCode: AppHelper.generateUniqueCode("ROLE"),
          roleName: request.roleName,
          authDirectAddress: request.authDirectAddress,
          isActive: true,
        },
        { transaction: dbTransaction }
      );

      if (!addMsRole?.id) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Unable to Add new Role`);

      let addMapRoleFeatureAccessStored: boolean[] =
        await this.insertMapRoleFeatureAccess(
          request.featureList,
          addMsRole.roleCode,
          dbTransaction
        );

      if (addMapRoleFeatureAccessStored.some((x) => x === false)) throw new Error(`${AppError.OOPS_SOMETHING_ISNT_QUITE_RIGHT} Unable to Add New Role Feature Access`);

      const response: BaseResponse<string> = {
        state: true,
        message: "New Role aded.",
        outputSchema: addMsRole.roleCode,
      };
      return response;
    } catch (error: any) {
      return AppHelper.baseHandleError(error?.message);
    }
  }

  public async EditRole(request: RoleEditRequest, dbTransaction: Transaction): Promise<BaseResponse<string>> {
    try {
    
      const roleDetail = await MsRole.findOne({
        attributes: ["roleCode", "roleName"],
        where: { roleCode: request.roleCode, isActive: true },
        raw: true,
      });

      if (!roleDetail) throw new Error(`Role not found.`);

      if((roleDetail.roleName === request.roleName) && (request?.featureList?.length || 0) === 0){
        throw new Error(`Role name has no change.`);
      }
      
      if(roleDetail.roleName !== request.roleName){
        const [updateMsRole] = await MsRole.update(
          {
            roleName: request.roleName,
            authDirectAddress: request.authDirectAddress
          },
          {
            where: {
              roleCode: request.roleCode,
            },
            transaction: dbTransaction,
          }
        );
        
        if (!updateMsRole) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Unable to Update Role`);
      }

      if (request?.featureList && (request?.featureList?.length || 0) > 0) {
        const isHasRoleFeature = await MapRoleFeatureAccess.count({
          where: {roleCode: request.roleCode},
        }) || 0;

        if(isHasRoleFeature > 0){
          const deleteMapRoleFeatureAccess = await MapRoleFeatureAccess.destroy({
            where: {
              roleCode: request.roleCode,
            },
            transaction: dbTransaction,
          });

          if (!deleteMapRoleFeatureAccess) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Unable to Remove Role Feature Access`);
        }

        let editMapRoleFeatureAccessStored: boolean[] = await this.insertMapRoleFeatureAccess(
          request.featureList,
          request.roleCode,
          dbTransaction
        );

        if (editMapRoleFeatureAccessStored.some((x) => x === false)) throw new Error(`${AppError.OOPS_SOMETHING_ISNT_QUITE_RIGHT} Unable to Add New Role Feature Access`);
      }

      const response: BaseResponse<string> = {
        state: true,
        message: "Role was edited.",
        outputSchema: request.roleCode,
      };
      return response;
    } catch (error: any) {
      return AppHelper.baseHandleError(error?.message);
    }
  }

  public async insertMapRoleFeatureAccess(featureList: RoleAddFeatureAccess[], roleCode: string, dbTransaction: Transaction): Promise<boolean[]> {
    if (!featureList || featureList.length === 0) return [];

    let addMapRoleFeatureAccessStored: boolean[] = [];
    for (const feature of featureList) {
      const msFeature: MsFeature | null = await MsFeature.findOne({
        where: { featureCode: feature.featureCode, isActive: true },
        raw: true,
      });

      if (
        !msFeature ||
        FeatureProps.some(
          (prop) =>
            !msFeature[prop as keyof MsFeature] &&
            feature[prop as keyof RoleAddFeatureAccess]
        )
      ) {
        addMapRoleFeatureAccessStored.push(false);
        continue;
      }

      const mapRoleFeatureAccess = await MapRoleFeatureAccess.create(
        {
          roleCode: roleCode,
          featureCode: feature.featureCode,
          reading: feature.reading,
          creating: feature.creating,
          updating: feature.updating,
          deleting: feature.deleting,
          activating: feature.activating,
          inactivating: feature.inactivating,
          importing: feature.importing,
          exporting: feature.exporting,
          approving: feature.approving,
          rejecting: feature.rejecting,
          special: feature.special,
        },
        { transaction: dbTransaction }
      );
      addMapRoleFeatureAccessStored.push(!!mapRoleFeatureAccess?.id);
    }

    return addMapRoleFeatureAccessStored;
  }

  public async deleteRole(request: RoleDeleteRequest, dbTransaction: Transaction): Promise<BaseResponse<string>> {
    try {
      const [updateMsRole] = await MsRole.update(
        {
          isActive: false,
        },
        {
          where: {
            roleCode: request.roleCode,
          },
          transaction: dbTransaction,
        }
      );

      if (!updateMsRole) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Unable to Remove Role`);

      const deleteMapUserRole = await MapUserRole.destroy({
        where: {
          roleCode: request.roleCode,
        },
        transaction: dbTransaction,
      });

      if (!deleteMapUserRole) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Unable to Remove User Role`);

      const deleteMapRoleFeatureAccess = await MapRoleFeatureAccess.destroy({
        where: {
          roleCode: request.roleCode,
        },
        transaction: dbTransaction,
      });

      if (!deleteMapRoleFeatureAccess) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Unable to Remove Role Feature Access`);

      const response: BaseResponse<string> = {
        state: true,
        message: "Role was removed.",
        outputSchema: request.roleCode,
      };
      return response;
    } catch (error: any) {
      return AppHelper.baseHandleError(error?.message);
    }
  }

  public async checkExistingRoleByField (field: string, value: string | number, errorMessage: string, errorWhenExist: boolean = true): Promise<string | null> {
    const count = await MsRole.count({
      where: { [field]: value },
    });

    if (errorWhenExist && count > 0) return errorMessage;
    if (!errorWhenExist && count === 0) return errorMessage;
    
    return null;
  };
}
