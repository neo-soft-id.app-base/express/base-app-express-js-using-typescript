import { Op, Transaction } from "sequelize";
import sequelize from "../config/db-config";
import { UserDetail, UserDetailRequest } from "../dto/user/user-detail.dto";
import { UserList, UserListRequest } from "../dto/user/user-list.dto";
import LkGender from "../models/lk_gender.model";
import MapUserRole from "../models/map_user_role.model";
import MsRole from "../models/ms_role.model";
import MsUser from "../models/ms_user.model";
import AppHelper from "../utils/helpers";
import { BaseResponse } from "../dto/base.dto";
import { UserAddRequest } from "../dto/user/user-add.dto";
import { AppError } from "../utils/app.const";
import { UserEditRequest } from "../dto/user/user-edit.dto";
import { UserDeleteRequest } from "../dto/user/user-delete.dto";
import RoleService from "./role.service";
import { ValidateUserData } from "../dto/user/user-validate.dto";

export default class UserService {
  public async getUserList(request: UserListRequest): Promise<BaseResponse<UserList[]>> {
    try {
      const search: string = request.search;
      const endData: number = +request.perPage;
      const startData: number = +request.perPage * +request.page - +request.perPage;

      const userList = await MapUserRole.findAll({
        attributes: [
          [sequelize.col("role.roleCode"), "roleCode"],
          [sequelize.col("role.roleName"), "roleName"],
          [sequelize.col("user.userCode"), "userCode"],
          [sequelize.col("user.firstName"), "firstName"],
          [sequelize.col("user.lastName"), "lastName"],
          [sequelize.col("user.username"), "username"],
          [sequelize.col("user.email"), "email"],
        ],
        include: [
          {
            model: MsUser,
            as: "user",
            required: false,
            right: true,
            attributes: []
          },
          {
            model: MsRole,
            as: "role",
            required: false,
            attributes: []
          },
        ],
        where: {
          [Op.or]: [
            { '$user.username$': { [Op.like]: `%${search}%` } },
            { '$user.firstName$': { [Op.like]: `%${search}%` } },
            { '$user.lastName$': { [Op.like]: `%${search}%` } },
            { '$role.roleName$': { [Op.like]: `%${search}%` } }
          ],
          '$user.isActive$': true
        },
        offset: startData,
        limit: endData,
        order: [
          [sequelize.col("user.firstName"), "ASC"],
          [sequelize.col("user.lastName"), "ASC"]
        ],
        raw: true,
      });
      
      if (!userList || (userList?.length || 0) === 0) throw new Error(`Users not found.`);

      const response: BaseResponse<UserList[]> = {
        state: true,
        message: "Ok.",
        outputSchema: AppHelper.cloneDeep(userList) as UserList[],
      };
      return response;
    } catch (error: any) {
      return AppHelper.baseHandleError(error?.message);
    }
  }

  public async getUserListTotalRecords(search: string): Promise<number | null> {
    const totalRecords = await MapUserRole.findAll({
      attributes: [
        [sequelize.col("user.userCode"), "userCode"]
      ],
      include: [
        {
          model: MsUser,
          as: "user",
          required: false,
          right: true,
          attributes: []
        },
        {
          model: MsRole,
          as: "role",
          required: false,
          attributes: []
        },
      ],
      where: {
        [Op.or]: [
          { '$user.username$': { [Op.like]: `%${search}%` } },
          { '$user.firstName$': { [Op.like]: `%${search}%` } },
          { '$user.lastName$': { [Op.like]: `%${search}%` } },
          { '$role.roleName$': { [Op.like]: `%${search}%` } }
        ], 
        '$user.isActive$': true 
      },
      order: [
        [sequelize.col("user.firstName"), "ASC"],
        [sequelize.col("user.lastName"), "ASC"]
      ],
      raw: true,
    }).then(x => x.length).catch(()=> 0);
    return totalRecords || 0;
  }

  public async getUserDetail(request: UserDetailRequest): Promise<BaseResponse<UserDetail>> {
    try {
      const userDetail = await MapUserRole.findOne({
        attributes: [
          [sequelize.col("role.roleCode"), "roleCode"],
          [sequelize.col("role.roleName"), "roleName"],
          [sequelize.col("user.userCode"), "userCode"],
          [sequelize.col("user.firstName"), "firstName"],
          [sequelize.col("user.lastName"), "lastName"],
          [sequelize.col("user.username"), "username"],
          [sequelize.col("user.phoneNumber"), "phoneNumber"],
          [sequelize.col("user.email"), "email"],
          [sequelize.col("user.genderId"), "genderId"],
          [sequelize.col("user.gender.genderName"), "genderName"],
          [sequelize.col("user.birthPlace"), "birthPlace"],
          [sequelize.col("user.birthDay"), "birthDay"],
          [sequelize.col("user.address"), "address"],
        ],
        include: [
          {
            model: MsUser,
            as: "user",
            include: [
              {
                model: LkGender,
                as: "gender",
                attributes: [],
                required: true,
              },
            ],
            required: false,
            right: true,
            attributes: [],
          },
          {
            model: MsRole,
            as: "role",
            required: false,
            attributes: [],
          },
        ],
        where: { 
          '$user.userCode$': request.userCode, 
          '$user.isActive$': true 
        },      
        raw: true,
      });
      
      if (!userDetail) throw new Error(`User not found.`);

      const response: BaseResponse<UserDetail> = {
        state: true,
        message: "Ok.",
        outputSchema: AppHelper.cloneDeep(userDetail) as UserDetail,
      };
      return response;
    } catch (error: any) {
      return AppHelper.baseHandleError(error?.message);
    }
  }

  public async addUser(request: UserAddRequest, dbTransaction: Transaction): Promise<BaseResponse<string>> {
    try {

      const validateUserData: ValidateUserData = { ...request || {}} as ValidateUserData
      const validateUserDataMessage = await this.validateUserData(validateUserData);
      if(validateUserDataMessage) throw new Error(validateUserDataMessage);

      const addMsUser = await MsUser.create(
        {
          userCode: AppHelper.generateUniqueCode("USER"),
          firstName: request.firstName,
          lastName: request?.lastName ?? "",
          username: request.username,
          password: AppHelper.md5(request.password),
          phoneNumber: request?.phoneNumber ?? "",
          email: request.email,
          genderId: request.genderId,
          birthPlace: request?.birthPlace ?? "",
          birthDay: request?.birthDay ?? "",
          address: request?.address ?? "",
          isActive: true,
        },
        { transaction: dbTransaction }
      );

      if (!addMsUser?.id) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Unable to Add new User`);

      const addMapUserRole = await MapUserRole.create(
        {
          userCode: addMsUser.userCode,
          roleCode: request.role.roleCode
        },
        { transaction: dbTransaction }
      );

      if (!addMapUserRole?.id) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Unable to Add new User Role`);

      const response: BaseResponse<string> = {
        state: true,
        message: "New User added.",
        outputSchema: addMsUser.userCode,
      };
      return response;
    } catch (error: any) {
      return AppHelper.baseHandleError(error?.message);
    }
  }

  public async editUser(request: UserEditRequest, dbTransaction: Transaction): Promise<BaseResponse<string>> {
    try {

      const userDetail = await MsUser.findOne({
        where: { userCode: request.userCode, isActive: true },
        raw: true,
      });

      if (!userDetail) throw new Error(`User not found.`);

      const representRequestUserOnly = AppHelper.cloneDeep(request) as Partial<UserEditRequest>;
      delete representRequestUserOnly.role;
      const updateUserData: Partial<MsUser> = {};
      for(const prop in representRequestUserOnly) {
        const isPasswordKey = (prop === 'password');
        const requestData = isPasswordKey
          ? AppHelper.md5(representRequestUserOnly[prop as keyof Partial<UserEditRequest>] as string)
          : representRequestUserOnly[prop as keyof Partial<UserEditRequest>];
        const userData = userDetail[prop as keyof MsUser];
        if((requestData === userData)) continue;

        updateUserData[prop as keyof MsUser] = requestData ?? null;
      }
      
      if(Object.values(updateUserData).length === 0) throw new Error(`User has no any property to change.`)
      
      const validateUserData: ValidateUserData = { ...request || {}} as ValidateUserData
      const validateUserDataMessage = await this.validateUserData(validateUserData);
      if(validateUserDataMessage) throw new Error(validateUserDataMessage);

      const [updateUser] = await MsUser.update(
        updateUserData,
        {
          where: {
            userCode: request.userCode
          },
          transaction: dbTransaction
        }
      );

      if (!updateUser) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Unable to Update User`);

      if(request?.role?.roleCode){  
        const roleService = new RoleService();
        const validateRoleMsg = await roleService.checkExistingRoleByField(
          "roleCode",
          request.role.roleCode,
          `Sorry, the role you entered does not exist.`,
          false
        );
        if (validateRoleMsg) throw new Error(validateRoleMsg);
        
        const deleteMapUserRole = await MapUserRole.destroy({
          where: {
            userCode: request.userCode
          },
          transaction: dbTransaction,
        });

        if (!deleteMapUserRole) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Failed to re-inserting user role`);
        
        const addMapUserRole = await MapUserRole.create(
          {
            userCode: request.userCode,
            roleCode: request.role.roleCode
          },
          { transaction: dbTransaction }
        );
  
        if (!addMapUserRole?.id) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Unable to Add new User Role`);
      }

      const response: BaseResponse<string> = {
        state: true,
        message: "User was edited.",
        outputSchema: request.userCode
      };
      return response;
    } catch (error: any) {
      return AppHelper.baseHandleError(error?.message);
    }
  }

  public async deleteUser(request: UserDeleteRequest, dbTransaction: Transaction): Promise<BaseResponse<string>> {
    try {
      const [updateMsUser] = await MsUser.update(
        {
          isActive: false,
        },
        {
          where: {
            userCode: request.userCode
          },
          transaction: dbTransaction,
        }
      );

      if (!updateMsUser) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Unable to Remove User`);

      const deleteMapUserRole = await MapUserRole.destroy({
        where: {
          userCode: request.userCode
        },
        transaction: dbTransaction,
      });

      if (!deleteMapUserRole) throw new Error(`${AppError.AN_UNEXPECTED_ERROR} Unable to Remove User Role`);

      const response: BaseResponse<string> = {
        state: true,
        message: "User was removed.",
        outputSchema: request.userCode,
      };
      return response;
    } catch (error: any) {
      return AppHelper.baseHandleError(error?.message);
    }
  }

  public async validateUserData(request: ValidateUserData): Promise<string | null>{
    if (request?.username) {
      const validateUsernameMsg = await this.checkExistingUserByField(
        "username",
        request.username,
        `Sorry, the username is already taken. Please choose another name.`
      );
      if (validateUsernameMsg) return validateUsernameMsg;
    }

    if (request?.email) {
      const validateEmailMsg = await this.checkExistingUserByField(
        "email",
        request.email,
        `Sorry, the E-mail is already taken. Please choose another E-mail address.`
      );
      if (validateEmailMsg) return validateEmailMsg;
    }

    if (request?.phoneNumber) {
      const validatePhoneNumbMsg = await this.checkExistingUserByField(
        "phoneNumber",
        request.phoneNumber,
        `Sorry, this phone number is already registered. If you need to update the phone number associated with this account, please reach Customer Support.`
      );
      if (validatePhoneNumbMsg) return validatePhoneNumbMsg;
    }

    if (request?.role?.roleCode) {
      const roleService = new RoleService();
      const validateRoleMsg = await roleService.checkExistingRoleByField(
        "roleCode",
        request.role.roleCode,
        `Sorry, the role you entered does not exist.`,
        false
      );
      if (validateRoleMsg) throw new Error(validateRoleMsg);
    }

    return null;
  }

  public async checkExistingUserByField (field: string, value: string | number, errorMessage: string, errorWhenExist: boolean = true): Promise<string | null> {
    const count = await MsUser.count({
      where: { [field]: value },
    });
    
    if (errorWhenExist && count > 0) return errorMessage;
    if (!errorWhenExist && count === 0) return errorMessage;

    return null;
  };

}
