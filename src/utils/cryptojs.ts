import CryptoJS from 'crypto-js';


export default class Encryption {
    public static set(value : string){
      var key = CryptoJS.enc.Utf8.parse(process.env.ENCRYPT_KEY!);
      var iv = CryptoJS.enc.Utf8.parse(process.env.IV_KEY!);
      var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(value.toString()), key,
      {
          keySize: 128 / 8,
          iv: iv,
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.Pkcs7
      });

      return encrypted.toString();
    }

    public static  get(value : string){
      var key = CryptoJS.enc.Utf8.parse(process.env.ENCRYPT_KEY!);
      var iv = CryptoJS.enc.Utf8.parse(process.env.IV_KEY!);
      var decrypted = CryptoJS.AES.decrypt(value, key, {
          keySize: 128 / 8,
          iv: iv,
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.Pkcs7
      });

      return decrypted.toString(CryptoJS.enc.Utf8);
    }
}
