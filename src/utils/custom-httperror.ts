
export default class CustomHttpError extends Error {
  public statusCode: number;

  constructor(message: string, statusCode: number) {
    super(message);
    this.name = 'HttpError';
    this.statusCode = statusCode;
    
    Object.setPrototypeOf(this, CustomHttpError.prototype);
  }
}
