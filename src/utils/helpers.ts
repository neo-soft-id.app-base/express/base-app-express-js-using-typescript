import crypto from "crypto";
import CustomHttpError from "./custom-httperror";
import { BaseResponse } from "../dto/base.dto";
import { AppError } from "./app.const";

export default class AppHelper {
  public static generateUniqueCode(prefix: string) {
    const currentDate = new Date();
    const options: Intl.DateTimeFormatOptions = {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
      timeZone: "Asia/Jakarta",
    };
    const formattedDate = currentDate
      .toLocaleString("id-ID", options)
      .replace(/[.,/ ]/g, "");

    const randomChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    let randomPart = "";
    for (let i = 0; i < 10; i++) {
      randomPart += randomChars.charAt(
        Math.floor(Math.random() * randomChars.length)
      );
    }

    return `${prefix}-${formattedDate}-${randomPart}`;
  }

  public static md5(str: string): string | null {
    if (!str) return null;
    return crypto.createHash("md5").update(str).digest("hex");
  }

  public static cloneDeep<T>(value: T): object | object[] | null {
    if (!value) return null;
    return JSON.parse(JSON.stringify(value));
  }

  public static isHasNoValue<T>(value: T) {
    if (typeof value === "string") return value === "";
    return value === undefined || value === null;
  }

  public static isHttpError<T>(error: T): boolean {
    return error instanceof CustomHttpError;
  }

  public static baseHandleError<T>(message?: string | null, falsyErrorMessage?: string | null): BaseResponse<T>{
    const falsyMessage: string = falsyErrorMessage ?? AppError.YIKES_IT_SEEMS_WEVE_HIT_A_BUMP_IN_THE_ROAD;
    const response: BaseResponse<T> = {
      state: false,
      message: message ?? falsyMessage,
      outputSchema: null,
    };
    return response;
  }
  
}
